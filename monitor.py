from Devices.RTDClick import RTDClick
from Devices.OneWire import OneWireSensor
from Plotter import Plotter

import time
import yaml
def main( ):
    print("\t\t\t============================")
    print("\t\t\t           MONITOR          ")
    print("\t\t\t============================")

    with open('labExcersiseConfig.yaml') as configFile:
        config = yaml.load(configFile, Loader=yaml.FullLoader)


    oneWireSensor = OneWireSensor(config ["OneWire"])
    jigSensors = RTDClick()
    monitor = Plotter()
    monitor.initMonitor()
    current_time = [0]
    sensorTemperature  = []
    peltierTemperature = []
    airTemperature     = []
    humidity           = []
    dewpoint           = []
    while True:
        temperatures = jigSensors.get_temperature()

        sensorTemperature.append(temperatures[0]) 
        peltierTemperature.append(temperatures[1])
        airTemperature.append(oneWireSensor.get_temperature())
        humidity.append(oneWireSensor.get_humidity())
        dewpoint.append(oneWireSensor.get_dewpoint())

        monitor.updateMonitor(current_time,sensorTemperature,peltierTemperature, airTemperature, dewpoint,humidity )

        time.sleep(1)
        current_time.append( current_time[-1] + 1 )

        if len(sensorTemperature) > 180:
            sensorTemperature.pop(0)
            peltierTemperature.pop(0)
            airTemperature.pop(0)
            humidity.pop(0)
            dewpoint.pop(0)
            current_time.pop(0)

if __name__ == '__main__':
    main()
