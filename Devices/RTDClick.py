"""The RTD Click module

The readout of RTD Click's is implemented in this module.
"""
import board
import busio
import digitalio
import adafruit_max31865
import logging

class RTDClick:
    
    def __init__(self):
        """Initialize the RTD Click's 

        This routine initializes the SPI communication, defines the Chip Select (CS) pins,
        the sensor type (PT1000,PT100) and the reference resistor.
        """
        self.pins = [5,6,22,27]
        self.type = 1000
        self.ref = 4320
        self.spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
        self.cs = [digitalio.DigitalInOut(self._get_alias(pin)) for pin in self.pins] 
    
    def get_temperature(self):
        """read out the RTD Click's"""
        dict_temp = []
        for cs in self.cs:
            temp = adafruit_max31865.MAX31865(self.spi,cs,wires=4,
                   rtd_nominal=self.type,ref_resistor=self.ref)
            dict_temp.append(float('{0:0.3f}'.format(temp.temperature)))
        return dict_temp
        
    def _get_alias(self,pin):
        if pin == 0:
            return board.D0
        elif pin == 1:
            return board.D1
        elif pin == 2:
            return board.D2
        elif pin == 3:
            return board.D3
        elif pin == 4:
            return board.D4
        elif pin == 5:
            return board.D5
        elif pin == 6:
            return board.D6
        elif pin == 7:
            return board.D7
        elif pin == 8:
            return board.D8
        elif pin == 9:
            return board.D9
        elif pin == 10:
            return board.D10
        elif pin == 11:
            return board.D11
        elif pin == 12:
            return board.D12
        elif pin == 13:
            return board.D13
        elif pin == 14:
            return board.D14
        elif pin == 15:
            return board.D15
        elif pin == 16:
            return board.D16
        elif pin == 17:
            return board.D17
        elif pin == 18:
            return board.D18
        elif pin == 19:
            return board.D19
        elif pin == 20:
            return board.D20
        elif pin == 22:
            return board.D22
        elif pin == 27:
            return board.D27

