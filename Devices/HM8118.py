from Devices.Utils import SerialPort
import numpy as np

class HM8118():
    def __init__(self, pSettings):
        pSettings["ReadTermination"] = "\r"
        self.serialPort = SerialPort( pSettings)

        self.initDevice()

    def initDevice(self):
        self.serialPort.write("*RST")
        self.serialPort.write("*WAI")
        self.serialPort.write("PMOD 3")
        self.serialPort.write("AVGM 0")
        #self.serialPort.write("NAVG 5")
        self.setFrequency(1000)

    def setFrequency( self, pFrequency ):
        self.serialPort.write( "FREQ {0:5d}".format(pFrequency) )

    def readCapacitance(self):
        values = []
        for _ in range(100):
            if len(values) >= 10:
                break
            self.serialPort.write("STRT")
            answer = self.serialPort.read("XALL?")
            print(answer)
            try:
                values.append(float(answer.split(',')[0]))
            except ValueError:
                print("Read back ERROR from HM8118")
        return np.mean(values)