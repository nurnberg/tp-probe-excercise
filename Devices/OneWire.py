import RPi.GPIO as gpio
import numpy as np

class OneWireSensor:
	"""The OneWire Humidity Sensor
	   The OneWire Humidity Sensor measures the relative humidity in percent.
	   This value is written into a file every 2 seconds and is used to
	   calculate the dew point.
	"""
	def __init__(self, pSettings):
		self._port 	= pSettings ["Port"]
		self.name	= pSettings ["Name"]
		if pSettings["Color"] == "Red":
			self.color = '\033[91m'
		elif pSettings["Color"] == "Green":
			self.color = '\033[92m'
		elif pSettings["Color"] == "Yellow":
			self.color = '\033[93m'
		elif pSettings["Color"] == "Blue":
			self.color = '\033[94m'
		elif pSettings["Color"] == "Magenta":
			self.color = '\033[95m'
		elif pSettings["Color"] == "Cyan":
			self.color = '\033[96m'
		else:
			self.color = ''
		self.cend   = '\033[0m'

	def read(self,file_name):
		file_object = open(file_name,'r')  
		line = file_object.read()
		print(self.color + self.name + ":\t" + line[:-1] + self.cend)
		return line	

	def _get_vad(self):
		self._port = self._port.rsplit('/',1)[0]+'/VAD'
		answer = float(self.read(self._port))
		return answer

	def _get_vdd(self):
		self._port = self._port.rsplit('/',1)[0]+'/VDD'
		answer = float(self.read(self._port))
		return answer

	def _get_temperature(self):
		self._port = self._port.rsplit('/',1)[0]+'/temperature'
		answer = float(self.read(self._port))
		return answer

	def get_temperature(self):
		"""
		Return the current temperature as measured by the DS2438 chip
		"""
		return self._get_temperature()

	def get_humidity(self):
		"""Get the relative humidity in %.
		This function measures the Honeywell sensor voltage and the air
		temperature and returns the relaitive humidity in percent.
		"""
		vad = self._get_vad()
		vdd = self._get_vdd()
		temperature = self._get_temperature()
		humidity = (((vad/vdd)-(0.8/vdd))/0.0062)/(1.0546-0.00216*temperature)
		if humidity > 100:
			humidity = 100
		elif humidity < 0:
			humidity = 0.1
		return humidity
	
	def get_dewpoint(self):
		"""calculate the dew point"""
		humidity = self.get_humidity()
		temp = self.get_temperature()
		if humidity > 100:
			humidity = 100
		if humidity <= 0:
			humidity = 0.1
		temperature = np.log(humidity/100) + 17.27*temp/(237.7 + temp)
		dew_point = 237.7*temperature/(17.27 - temperature)
		return dew_point

