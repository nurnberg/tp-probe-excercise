import RPi.GPIO as gpio

class HumiditySensor:
	"""The OneWire Humidity Sensor
	   The OneWire Humidity Sensor measures the relative humidity in percent.
	   This value is written into a file every 2 seconds and is used to
	   calculate the dew point.
	"""

	def __init__(self):
            """ Init the device """
            self._port = '/sys/bus/w1/devices/81-000000383240/'

	def read(self,file_name):
	    file_object = open(file_name,'r')  
	    line = file_object.read()
	    print(line)
	    return line	

	def _get_vad(self):
	    self._port = self._port.rsplit('/',1)[0]+'/vad'
	    answer = float(self.read(self._port))/100.
	    return answer

	def _get_vdd(self):
	    self._port = self._port.rsplit('/',1)[0]+'/vdd'
	    answer = float(self.read())/100.
	    return answer

	def _get_temperature(self):
	    self._port = self._port.rsplit('/',1)[0]+'/temperature'
	    answer = float(self.read())/256.
	    return answer

	def get_temperature(self):
	    """
	    Return the current temperature as measured by the DS2438 chip
	    """
	    return self._get_temperature()

	def get_humidity(self):
	    """Get the relative humidity in %.
	    This function measures the Honeywell sensor voltage and the air
	    temperature and returns the relaitive humidity in percent.
	    """
	    vad = self._get_vad()
	    vdd = self._get_vdd()
	    temperature = self._get_temperature()
	    humidity = (((vad/vdd)-(0.8/vdd))/0.0062)/(1.0546-0.00216*temperature)
	    if humidity > 100:
	        humidity = 100
	    elif humidity < 0:
	        humidity = 0.1
	    return humidity

test = HumiditySensor()
print(test.get_temperature())
print(test.get_humidity())

