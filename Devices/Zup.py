from Devices.Utils import SerialPort

class Zup():
    def __init__(self, pSettings):
        pSettings["ReadTermination"] = "\r\n"
        self.serialPort = SerialPort( pSettings)

        self.initDevice()

    def initDevice(self):
        self.serialPort.write(":ADR{0:02d};".format(1))
        self.serialPort.write(":DCL;")
        self.serialPort.write(":RMT1;")
        self.serialPort.write(":OUT0;")

        current_compliance = 10.0
        self.serialPort.write(":OVP{:04.1f};".format(9.5) )
        self.serialPort.write(":UVP{:04.1f};".format(0.0) )
        self.serialPort.write(":CUR{:06.3f};".format(current_compliance))

        self.serialPort.write(":VOL{:06.3f};".format(0.0))
        self.serialPort.write(":OUT1;")

    def set_voltage(self, voltage):
        self.serialPort.write(":VOL{:06.3f};".format(voltage))

    def read_current(self):
        answer = self.serialPort.read(":CUR?;")
        answer = answer.strip("AA")
        return float(answer)

    def ramp_down(self):
        self.serialPort.write(":DCL;")
        self.set_voltage(0)

    def stop(self):
        """ Stop device by ramping down the voltage and closing the output """
        self.ramp_down()
        self.serialPort.write(":OUT0;")
