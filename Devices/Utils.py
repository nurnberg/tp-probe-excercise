import serial
import time
class SerialPort():

    def __init__( self, pSettings ):
        self.port = serial.Serial(  port=pSettings["Port"],
                                    baudrate=pSettings["BaudRate"],
                                    parity=serial.PARITY_NONE,
                                    stopbits=serial.STOPBITS_ONE,
                                    bytesize=serial.EIGHTBITS,
                                    xonxoff= True)

        self.name   = pSettings["Name"]
        if pSettings["Color"] == "Red":
            self.color = '\033[91m'
        elif pSettings["Color"] == "Green":
            self.color = '\033[92m'
        elif pSettings["Color"] == "Yellow":
            self.color = '\033[93m'
        elif pSettings["Color"] == "Blue":
            self.color = '\033[94m'
        elif pSettings["Color"] == "Magenta":
            self.color = '\033[95m'
        elif pSettings["Color"] == "Cyan":
            self.color = '\033[96m'
        else:
            self.color = ''
        self.cend   = '\033[0m'

        print(self.color + '\33[1m' + "Initialize " + pSettings["Name"] + self.cend)
        self.read_termination = pSettings["ReadTermination"]

    def write( self , pCmd):
        self.port.write(bytes('{0}{1}'.format(pCmd, "\r\n"), 'utf-8'))
        time.sleep(0.15)
        print(self.color + self.name + ":\t" + pCmd + self.cend)

    def read( self ,pCmd):
        self.write(pCmd)
        buffer = bytearray()
        while True:
            one_byte = self.port.read(1)
            buffer.extend(one_byte)
            if bytearray(self.read_termination, "utf-8") in buffer:
                return bytes(buffer).decode("utf-8").strip(self.read_termination)

            if not one_byte:
                self.port.close()
                return None
