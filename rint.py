""" Script to measure R_int value"""
#pylint: disable=invalid-name

import time
import yaml
import numpy as np
from Devices.Keithley6517A import Keithley6517a
from Devices.Zup import Zup
from Plotter import Plotter


def main():
    """main"""
    print("\t\t\t=============================")
    print("\t\t\tMeasure R_int characteristics")
    print("\t\t\t=============================")

    with open('labExcersiseConfig.yaml') as cfg_file:
        config = yaml.load(cfg_file, Loader=yaml.FullLoader)

    lvSupply = Zup(config["ZupLV"])
    hvSupply = Keithley6517a(config["Keithley6517A"])
    hvSupply.initDevice(pCompliance="1E-8")

    #start stop step
    start = config["Measurements"]["R_int"]["min"]
    stop = config["Measurements"]["R_int"]["max"]
    step = config["Measurements"]["R_int"]["step"]

    x = []
    y = []
    newPlotter = Plotter()
    newPlotter.initRintPlot()

    #Deplete sensor
    hvSupply.ramp_voltage(config["Measurements"] \
                          ["R_int"]["biasvoltage"])

    with open('Data/R_int_data.txt', 'w') as dat_file:
        dat_file.write("Voltage (V)\tCurrent(A)\n")
        for voltage in [round(i, 1) for i in np.arange(start, stop, step)]:
            lvSupply.set_voltage(voltage)
            time.sleep(0.2)
            print("LV Voltage: " + str(voltage))
            current = hvSupply.read_current()
            print("Current:\t" + str(current))
            x.append(voltage)
            y.append(current)
            newPlotter.updateRintPlot(x, y)

    lvSupply.stop()
    hvSupply.stop()
    newPlotter.displayPlot()


if __name__ == '__main__':
    main()
