"""Script to measure the coupling capacitance"""
#pylint: disable=invalid-name

import time
import yaml
from Devices.Keithley6517A import Keithley6517a
from Devices.HM8118 import HM8118

def main():
    """main"""
    print("\t\t\t============================")
    print("\t\t\tMeasure CC characteristics")
    print("\t\t\t============================")

    #load config file
    with open('labExcersiseConfig.yaml') as configFile:
        config = yaml.load(configFile, Loader=yaml.FullLoader)

    #initialize HVSupply
    print("Initialize Keithley6517A")
    HVSupply = Keithley6517a(config["Keithley6517A"])
    HVSupply.initDevice()

    #initialize LCR meter
    print("Initialize HM8118")
    LCR = HM8118(config["HM8118"])
    LCR.setFrequency(config["Measurements"]["CC"]["frequency"])

    #ramp HV
    HVSupply.ramp_voltage(config["Measurements"]["CC"]["biasvoltage"])
    time.sleep(20)

    #open data file and read capacitance
    with open('Data/CC_data.txt', 'w') as dat_file:
        dat_file.write("Coupling capacitance (F)\n")
        capacitance = LCR.readCapacitance()
        print(f"Coupling capacitance: {capacitance}")
        dat_file.write(str(capacitance))

    #rampdown
    HVSupply.ramp_down()

if __name__ == '__main__':
    main()
