"""Script to measure C(V) characteristics"""
#pylint: disable=invalid-name

import time
import yaml
import numpy as np
from Devices.Keithley6517A import Keithley6517a
from Devices.HM8118 import HM8118
from Devices.IsoBox import IsoBox
from Plotter import Plotter

def main():
    """main"""
    print("\t\t\t============================")
    print("\t\t\tMeasure C(V) characteristics")
    print("\t\t\t============================")

    with open('labExcersiseConfig.yaml') as configFile:
        config = yaml.load(configFile, Loader=yaml.FullLoader)

    print("Initialize Keithley6517A")
    HVSupply = Keithley6517a(config["Keithley6517A"])
    HVSupply.initDevice()

    print("Initialize HM8118")
    LCR = HM8118(config["HM8118"])
    LCR.setFrequency(1000)

    print("Initialize IsoBox")
    myIsoBox = IsoBox(config["IsoBox"])
    myIsoBox.setDischargeState()
    myIsoBox.setChargeState()

    #start stop step
    start = config["Measurements"]["CV"]["min"]
    stop = config["Measurements"]["CV"]["max"]
    step = config["Measurements"]["CV"]["step"]

    x = []
    y = []
    newPlotter = Plotter()
    newPlotter.initCvPlot()

    with open('Data/CV_data.txt', 'w') as dat_file:
        dat_file.write("Voltage (V)\tCapacitance(C)\n")

        for voltage in [round(i, 1) for i in np.arange(start, stop, step)]:
            HVSupply.set_voltage(voltage)
            x.append(voltage)
            time.sleep(2)
            c = LCR.readCapacitance()
            print(c)
            y.append(float(c))
            newPlotter.updateCvPlot(x, y)
            print("V:\t" + str(voltage) + "\tC:\t" + str(c))
            data = str(voltage) + '\t' + str(c)
            dat_file.write(data + '\n')
            time.sleep(0.5)

    HVSupply.ramp_down(2)
    myIsoBox.setDischargeState()
    newPlotter.displayPlot()

if __name__ == '__main__':
    main()
