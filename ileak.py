"""Script to measure the strip leakage current"""
#pylint: disable=invalid-name

import yaml
from Devices.Keithley6517A import Keithley6517a

def main():
    """main"""
    print("\t\t\t============================")
    print("\t\t\tMeasure I_leak characteristics")
    print("\t\t\t============================")

    #load config file
    with open('labExcersiseConfig.yaml') as cfg_file:
        config = yaml.load(cfg_file, Loader=yaml.FullLoader)

    #initialize HVSupply
    print("Initialize Keithley6517A")
    HVSupply = Keithley6517a(config["Keithley6517A"])
    HVSupply.initDevice()

    #ramp HV
    HVSupply.ramp_voltage(config["Measurements"]["I_leak"]["biasvoltage"])

    #open data file and read capacitance
    with open('Data/I_leak_data.txt', 'w') as dat_file:
        dat_file.write("Strip leakage current (A)\n")
        current = HVSupply.read_current()
        print(f"Leakage Current: {current}")
        dat_file.write(str(current))

    #rampdown
    HVSupply.ramp_down()

if __name__ == '__main__':
    main()
